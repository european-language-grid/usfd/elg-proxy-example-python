Example ELG Proxy
================

This is a simple Python application that listens for requests in the ELG API
format, dispatches them to a remote API of some kind, and converts the
responses back into ELG format. The server is implemented using Quart and the
API client side with aiohttp, so the process is fully asyncio driven and one
instance can serve many simultaneous API calls.
