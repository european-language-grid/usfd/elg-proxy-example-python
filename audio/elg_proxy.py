#!/usr/bin/env python3
#
#    Copyright 2020 University of Sheffield
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#
# Proxy that adapts a third-party API to the ELG one.  This example supports
# the "audio" request type.
#

from quart import Quart, request
from werkzeug.exceptions import BadRequest, RequestEntityTooLarge
import aiohttp
import os
import audio_request

import traceback

app = Quart(__name__)
app.config["JSON_SORT_KEYS"] = False
if os.environ.get('REQUEST_SIZE_LIMIT') is not None:
    app.config['MAX_CONTENT_LENGTH'] = int(os.environ['REQUEST_SIZE_LIMIT'])

endpoint = os.environ.get('ENDPOINT_URL', 'http://www.example.com/some/api')

class ProcessingError(Exception):

    def __init__(self, status_code, code, text, *params):
        self.status_code = status_code
        self.code = code
        self.text = text
        self.params = params

    @staticmethod
    def InternalError(text):
        return ProcessingError(500, "elg.service.internalError", "Internal error during processing: {0}", text)

    @staticmethod
    def InvalidRequest():
        return ProcessingError(400, "elg.request.invalid", "Invalid request message")

    @staticmethod
    def TooLarge():
        return ProcessingError(413, "elg.request.too.large", "Request size too large")

    @staticmethod
    def UnsupportedMime(mime):
        return ProcessingError(400, "elg.request.text.mimeType.unsupported", "MIME type {0} not supported by this service", mime)

    @staticmethod
    def UnsupportedType(request_type):
        return ProcessingError(400, "elg.request.type.unsupported", "Request type {0} not supported by this service", request_type)

    def to_json(self):
        return {
            "failure": {
                "errors": [{
                    "code": self.code,
                    "text": self.text,
                    "params": self.params,
                }]
            }
        }

@app.errorhandler(ProcessingError)
def error_message(err):
    """Handler for application exceptions, mapping them to an ELG failure
    message."""
    return err.to_json(), err.status_code

@app.errorhandler(BadRequest)
def bad_request_error(err):
    """Handler for the internal BadRequest exception, raised if the incoming
    request body claims to be JSON but fails to parse."""
    return error_message(ProcessingError.InvalidRequest())

@app.errorhandler(RequestEntityTooLarge)
def bad_request_error(err):
    """Handler for the internal RequestEntityTooLarge exception, raised if
    the incoming request body exceeds the configured maximum size."""
    return error_message(ProcessingError.TooLarge())

@app.route("/healthz")
def healthcheck():
    """Simple health check endpoint"""
    return {'alive':True}

@app.before_serving
async def setup():
    """One-time setup tasks that must happen before the first request is
    handled, but require access to the event loop so cannot happen at the top
    level."""
    # Create the shared aiohttp session
    global session
    session = aiohttp.ClientSession()
    # or you may wish to configure things like default headers, e.g.
    # session = aiohttp.ClientSession(headers = {'X-API-Key':os.environ.get('APIKEY')})

@app.after_serving
async def shutdown():
    """Logic that must run at shutdown time, after the last request has been
    handled."""
    if session is not None:
        await session.close()

@app.route("/process", methods=["POST"])
async def process_text():
    """Main request processing logic - accepts a multipart request with the
    JSON request metadata as the first part named "request" and the audio content
    as the next part named "content", passes the audio to the remote API and maps
    its response back into ELG format."""

    # "data" is the parsed JSON request, "content" is an async generator
    # yielding chunks of bytes containing the audio
    data, content = await audio_request.parse()

    # sanity checks on the request message
    if (data.get('type') != 'audio'):
        raise ProcessingError.UnsupportedType()

    try:
        # Make the remote call
        async with session.post(endpoint, data=content) as client_response:
            status_code = client_response.status
            response_data = await client_response.json()
    except:
        traceback.print_exc()
        raise ProcessingError.InternalError('Error calling API')

    if status_code >= 400:
        # if your API returns sensible error messages you could include that
        # instead of the generic message
        raise ProcessingError.InternalError('Error calling API')

    # Successful response from the remote API, so build a suitable ELG response
    # for your service here
    return {
        'response':{
            'type':'annotations',
            'annotations':{
                'Example':[
                    {'start':0, 'end':len(elg_request['content']), 'features':{} }
                ]
            }
        }
    }


if __name__ == '__main__':
    app.run()
