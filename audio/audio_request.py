#!/usr/bin/env python3
#
#    Copyright 2020 University of Sheffield
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Helper functions to parse an ELG multipart audio request in streaming
# fashion.
#
import json
from quart import request, current_app
from werkzeug.exceptions import BadRequest, RequestEntityTooLarge
from werkzeug.http import parse_options_header
from werkzeug.sansio.multipart import Data, Epilogue, Field, File, MultipartDecoder, NeedData

async def parse():
    """Parse an ELG audio request body.  Returns a tuple (request_json, data)
    where request_json is a dict representing the parsed JSON of the "request"
    part of the multipart request, and data is an async generator yielding the
    audio data in chunks."""

    if request.mimetype == 'multipart/form-data':
        return await parse_multipart()
    elif request.mimetype == 'audio/mpeg':
        return await parse_plain('MP3')
    elif request.mimetype == 'audio/wav' or request.mimetype == 'audio/x-wav':
        return await parse_plain('LINEAR16')
    else:
        raise BadRequest()

async def parse_plain(audioformat):
    return {'type':'audio', 'format':audioformat}, request.body

async def parse_multipart():
    boundary = request.mimetype_params.get("boundary", "").encode("ascii")
    if not boundary:
        raise BadRequest()

    # Logic: the request should consist of first a "form field" part named
    # "request" containing JSON, and second a "file upload" part named
    # "content" containing the audio.  This generator fully parses the JSON
    # part and yields that as a dict, then subsequently yields chunks of the
    # audio data until they run out.  We create the generator and consume its
    # first yield (the parsed JSON), then return the active generator so the
    # rest of the binary chunks can be consumed by the caller in an async for.
    # This logic is heavily inspired by quart.formparser but we don't use that
    # directly as it would attempt to buffer the binary data rather than
    # allowing it to stream directly from the request.
    async def multipart_gen():
        parser = MultipartDecoder(boundary, current_app.config['MAX_CONTENT_LENGTH'])

        found_request = False
        request_buf = []
        in_content = False
        async for data in request.body:
            parser.receive_data(data)
            event = parser.next_event()
            while not isinstance(event, (Epilogue, NeedData)):
                if isinstance(event, Field):
                    # this should be the "request" section
                    if event.name != 'request':
                        raise BadRequest()
                    found_request = True
                    request_charset = 'utf-8'
                    request_content_type = event.headers.get("content-type")
                    if request_content_type:
                        mimetype, ct_params = parse_options_header(request_content_type)
                        if mimetype != 'application/json':
                            raise BadRequest()
                        request_charset = ct_params.get('charset', request_charset)
                elif isinstance(event, File):
                    if not found_request:
                        raise BadRequest()
                    # this should be the "content" section
                    if event.name != 'content':
                        raise BadRequest()
                    in_content = True
                elif isinstance(event, Data):
                    if in_content:
                        # we're streaming the content now
                        yield event.data
                    elif found_request:
                        request_buf.append(event.data)
                        if not event.more_data:
                            # finished the request section, so parse it
                            try:
                                yield json.loads(b"".join(request_buf).decode(request_charset))
                            except:
                                raise BadRequest()
                            # allow the JSON buffer to be freed by the GC
                            request_buf = []
                event = parser.next_event()

    mp_gen = multipart_gen()

    # generator yields first the parsed JSON request, then the content as
    # chunks of bytes
    return (await mp_gen.asend(None)), mp_gen
